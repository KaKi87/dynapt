import { Readable } from 'node:stream';

import {
    join as joinPath
} from 'https://deno.land/std@0.224.0/path/mod.ts';
import { exists } from 'https://deno.land/std@0.224.0/fs/mod.ts';
import PQueue from 'npm:p-queue@^8';
import axios from 'npm:axios@^1.7.2';
import { pipe } from 'https://git.kaki87.net/KaKi87/pipeline-ish/raw/branch/master/main.js';
import picomatch from 'npm:picomatch@^4.0.2';

import dirname from '../dirname.js';
import createDebFromAppimage from '../lib/createDebFromAppimage.js';

import { getConfig } from './getConfig.js';

const
    dataPath = joinPath(dirname, './data'),
    cachePath = joinPath(dirname, './cache.json');

export const updatePackages = async () => {
    console.log('· Updating packages');
    if(!await exists(dataPath))
        await Deno.mkdir(dataPath);
    const
        t1 = Date.now(),
        {
            concurrency,
            apps
        } = await getConfig(),
        queue = new PQueue({ ...concurrency ? { concurrency } : {} });
    let cache;
    try {
        cache = await Deno.readTextFile(cachePath);
    }
    catch {}
    cache = cache ? JSON.parse(cache) : {};
    const packagesData = (await Promise.all(apps.map(async app => {
        const response = await queue.add(async () => {
            console.log(`├── Fetching '${app.name}'`);
            const
                url =
                    app.url    ? app.url :
                    app.github ? `https://api.github.com/repos/${app.github.repo}/releases/latest` :
                                 undefined,
                etag = cache[app.id]?.etag;
            let response;
            try {
                response = await axios({
                    url,
                    headers: {
                        ...etag ? {
                            'if-none-match': etag
                        } : {}
                    },
                    ...app.url ? {
                        responseType: 'stream'
                    } : {},
                    validateStatus: status => [200, 304].includes(status)
                });
            }
            catch(error){
                return console.error(`├── Failed '${app.name}' : ${error.satck || error}`);
            }
            const isUnmodified =
                app.url    ? response.status === 304 :
                app.github ? response.status === 304 || cache[app.id]?.id === response.data['id'] :
                             undefined;
            if(isUnmodified)
                return console.log(`├── Canceled '${app.name}' : unmodified`);
            console.log(`├── Fetched '${app.name}'`);
            return response;
        });
        if(!response){
            const packages = [];
            for await (const item of Deno.readDir(dataPath)){
                if(item.isFile && item.name.startsWith(`${app.id}-`) && item.name.endsWith('.package'))
                    await pipe(
                        joinPath(dataPath, item.name),
                        Deno.readTextFile,
                        packages.push
                    );
            }
            return packages.join('');
        }
        const
            files = [],
            fileExtension = app.appimage ? '.appimage' : '.deb';
        if(app.url)
            files.push({ getStream: () => response.data });
        else if(app.github)
            files.push(
                ...response
                    .data['assets']
                    .filter(item =>
                        item['name'].toLowerCase().endsWith(fileExtension)
                        &&
                        (!app.github.filter || picomatch(app.github.filter, { nocase: true })(item['name']))
                    )
                    .map(item => ({
                        getStream: async () => (await axios({
                            url: item['browser_download_url'],
                            responseType: 'stream'
                        })).data,
                        creationTimestamp: new Date(item['created_at']).getTime() / 1000,
                        tagName: response.data['tag_name']
                    }))
            );
        return await queue.addAll(files.map((file, fileIndex) => async () => {
            const
                appText = `'${app.name}' (file #${fileIndex + 1})`,
                fileBaseName = `${app.id}-${fileIndex}`,
                outputPath = joinPath(dataPath, fileBaseName);
            await Deno.mkdir(outputPath, { recursive: true });
            let filePath = joinPath(outputPath, `${fileBaseName}${fileExtension}`);
            console.log(`├── Downloading ${appText}`);
            try {
                const fileStream = await pipe(
                    file.getStream(),
                    Readable.toWeb
                );
                await pipe(
                    Deno.open(filePath, { create: true, write: true }),
                    _ => _.writable,
                    _ => fileStream.pipeTo(_)
                );
            }
            catch(error){
                return console.error(`├── Failed ${appText} : ${error.satck || error}`);
            }
            console.log(`├── Downloaded ${appText}\n│   Generating ${app.appimage ? 'DEB' : 'package'}`);
            if(app.appimage){
                await new Deno.Command('chmod', { args: ['+x', filePath] }).output();
                try {
                    filePath = await createDebFromAppimage({
                        inputPath: filePath,
                        outputPath,
                        fallbackVersion: file.tagName?.match(/^v?([0-9].+)/)?.[1] || file.creationTimestamp / 1000,
                        bin: app.appimage.bin
                    });
                }
                catch(error){
                    return console.error(`├── Failed ${appText} : ${error.stack || error}`);
                }
                console.log(`├── DEB generated for ${appText}\n│   Generating package`);
            }
            const packageData = await pipe(
                new Deno.Command(
                    'dpkg-scanpackages',
                    {
                        args: [`./data/${fileBaseName}`],
                        cwd: dirname
                    }
                ),
                _ => _.output(),
                _ => _.stdout,
                _ => new TextDecoder().decode(_),
                _ => _.replace(/^(Filename: ).+(data\/.+)/m, '$1$2')
            );
            await pipe(
                joinPath(dataPath, `${fileBaseName}.package`),
                _ => Deno.writeTextFile(_, packageData)
            );
            console.log(`├── Package generated for ${appText}`);
            if(!cache[app.id])
                cache[app.id] = {};
            cache[app.id].etag = response.headers['etag'];
            if(app.github)
                cache[app.id].id = response.data['id'];
            return packageData;
        }));
    }))).join('');
    await pipe(
        joinPath(dataPath, 'Packages'),
        _ => Deno.writeTextFile(_, packagesData)
    );
    const releaseData = await pipe(
        new Deno.Command(
            'apt-ftparchive',
            {
                args: ['release', '.'],
                cwd: dataPath
            }
        ),
        _ => _.output(),
        _ => _.stdout,
        _ => new TextDecoder().decode(_)
    );
    await pipe(
        JSON.stringify(cache, null, 4),
        _ => Deno.writeTextFile(cachePath, _)
    );
    const t2 = Date.now();
    console.log(`└── Completed in ${t2 - t1}ms`);
    return {
        packagesData,
        releaseData
    };
};