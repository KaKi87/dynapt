import { pipe } from 'https://git.kaki87.net/KaKi87/pipeline-ish/raw/branch/master/main.js';
import { join as joinPath } from 'https://deno.land/std@0.224.0/path/mod.ts';
import AsciiTable from 'npm:ascii-table@0.0.9';

import dirname from '../dirname.js';
import { getConfig } from './getConfig.js';

const dataPath = joinPath(dirname, './data');

export default async () => {
    let dataFiles;
    try {
        dataFiles = await pipe(
            Deno.readDir(dataPath),
            Array.fromAsync
        );
    }
    catch {}
    const
        { apps } = await getConfig(),
        packages = dataFiles ? await Promise.all(
            dataFiles
                .filter(({ name }) => name.endsWith('.package'))
                .map(({ name }) => (async () => {
                    const data = await pipe(
                        joinPath(dataPath, name),
                        Deno.readTextFile,
                        _ => _.split('\n')
                    );
                    return {
                        fileName: name,
                        name: data[0].slice(9),
                        version: data[1].slice(9),
                        architecture: data[2].slice(14)
                    };
                })())
        ) : [];
    if(packages) for(
        const line
        of await pipe(
            new Deno.Command('apt', { args: ['list', '--installed', ...packages.map(_ => _.name)] }),
            _ => _.output(),
            _ => _.stdout,
            _ => new TextDecoder().decode(_).split('\n')
        )
    ){
        const [name,, version, architecture] = line.split(/[/ ]/);
        const packageData = packages.find(packageData => packageData.name === name && packageData.architecture === architecture);
        if(packageData)
            packageData.installedVersion = version;
    }

    console.log(
        new AsciiTable()
            .setHeading('App', 'Package name', 'Package architecture', 'Available version', 'Installed version')
            .addRowMatrix(
                apps.flatMap(app => {
                    const appPackages = packages.filter(({ fileName }) => fileName.startsWith(`${app.id}-`));
                    if(!appPackages.length)
                        return [[app.name, ' ', ' ', ' ', ' ']];
                    return appPackages.map(packageData => [app.name, packageData.name, packageData.architecture, packageData.version, packageData.installedVersion]);
                })
            )
            .toString()
    );
};