import { join as joinPath } from 'https://deno.land/std@0.224.0/path/mod.ts';
import createFastify from 'npm:fastify@^4';
import { pipe } from 'https://git.kaki87.net/KaKi87/pipeline-ish/raw/branch/master/main.js';

import dirname from '../dirname.js';
import { getConfig } from '../src/getConfig.js';
import { updatePackages } from '../src/updatePackages.js';

export const serve = async () => {
    {
        const
            prependMetadata = (message, isError) => {
                const metadata = `${isError ? '[!]' : '   '} [${new Date().toLocaleString()}] `;
                return `${metadata}${(message?.stack || message)?.toString().split('\n').join(`\n${' '.repeat(metadata.length)}`)}`;
            },
            consoleLog = console.log,
            consoleError = console.error,
            writeLogFile = message => Deno.writeTextFile(
                joinPath(dirname, './dynapt.log'),
                `${message}\n`,
                {
                    create: true,
                    append: true
                }
            );
        console.log = message => {
            message = prependMetadata(message);
            consoleLog(message);
            writeLogFile(message);
        };
        console.error = message => {
            if(message === 'Not implemented: Server.setTimeout()') return;
            message = prependMetadata(message, true);
            consoleError(message);
            writeLogFile(message);
        };
    }

    const
        {
            port
        } = await getConfig(),
        fastify = createFastify();

    fastify.addHook('preHandler', (request, _, done) => {
        console.log(`${request.method} ${request.url}`);
        done();
    });
    fastify.setErrorHandler((error, _, reply) => {
        console.error(error);
        reply.status(500).send();
    });

    let updatingPackages, updatingPackagesTimeout;

    fastify.get(
        '/Packages',
        async () => {
            if(!updatingPackages)
                updatingPackages = updatePackages();
            const { packagesData } = await updatingPackages;
            clearTimeout(updatingPackagesTimeout);
            updatingPackagesTimeout = setTimeout(() => updatingPackages = undefined, 60000);
            return packagesData;
        }
    );

    fastify.get(
        '/Release',
        async () => {
            if(!updatingPackages)
                updatingPackages = updatePackages();
            const { releaseData } = await updatingPackages;
            clearTimeout(updatingPackagesTimeout);
            updatingPackagesTimeout = setTimeout(() => updatingPackages = undefined, 60000);
            return releaseData;
        }
    );

    fastify.get(
        '/data/:directoryName/:fileName',
        (request, reply) => pipe(
            joinPath(dirname, `./data/${request.params.directoryName}/${request.params.fileName}`),
            Deno.readFile,
            _ => reply.send(_)
        )
    );

    await fastify.listen({ port });
    console.log(`Listening to localhost:${port}`);
};