import { join as joinPath } from 'https://deno.land/std@0.224.0/path/mod.ts';
import { pipe } from 'https://git.kaki87.net/KaKi87/pipeline-ish/raw/branch/master/main.js';
import Joi from 'npm:joi@^17';

import dirname from '../dirname.js';

const configPath = joinPath(dirname, './config.json');

export const getConfig = async () => {
    let configText, isChanged;
    try {
        configText = await Deno.readTextFile(configPath);
    }
    catch {
        console.log('Not configured');
        Deno.exit(1);
    }
    const config = await pipe(
        configText,
        JSON.parse,
        _ => Joi.object({
            port: Joi.number().integer().positive().required(),
            concurrency: Joi.number().integer().positive().allow(null),
            apps: Joi.array().items(
                Joi
                    .object({
                        name: Joi.string().required(),
                        id: Joi.string().uuid().default(() => {
                            isChanged = true;
                            return crypto.randomUUID();
                        }),
                        url: Joi.string().uri({ scheme: 'https' }),
                        github: Joi.object({
                            repo: Joi.string().regex(/^[\w.-]+\/[\w.-]+$/).required(),
                            filter: Joi.string()
                        }),
                        appimage: [
                            Joi.boolean().valid(true),
                            Joi.object({
                                bin: Joi.string()
                            }).min(1)
                        ]
                    })
                    .xor('url', 'github')
            )
        }).validateAsync(_)
    );
    if(isChanged)
        await pipe(
            JSON.stringify(config, null, 4),
            _ => Deno.writeTextFile(configPath, _)
        );
    return config;
};