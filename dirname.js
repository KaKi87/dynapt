import { join as joinPath } from 'https://deno.land/std@0.224.0/path/mod.ts';

export default
    Deno.args.includes('--is-compiled')
        ? joinPath(Deno.execPath(), '..')
        : import.meta.dirname;