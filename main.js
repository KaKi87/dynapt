import { program, Option } from 'npm:commander@^12';

import { serve } from './src/server.js';
import dirname from './dirname.js';
import list from './src/list.js';
import { updatePackages } from './src/updatePackages.js';
import extractAppimage from './lib/extractAppimage.js';

const version = _ => console.log(`dynapt ${program.opts().setVersion}`);

program
    .name('dynapt')
    .description('Dynamic APT repository')
    .allowExcessArguments(false)
    .addOption(new Option('--is-compiled').hideHelp())
    .addOption(new Option('--set-version <version>').hideHelp())
    .option('-v, -V, --version', 'Print version')
    .addOption(new Option('--version').hideHelp())
    .action(options => {
        if(options.V || options.version) return version(options);
        serve();
    });

program
    .command('version', { hidden: true })
    .description('Print version')
    .action(version);

program
    .command('serve')
    .description('Run APT repository server')
    .action(serve);

program
    .command('clean')
    .description('Clean cache')
    .action(async () => {
        // Wildcard doesn't work without bash.
        await new Deno.Command('bash', { args: ['-c', 'rm -r ./cache.json ./data/*'], cwd: dirname }).output();
        console.log('✓ Cache cleaned');
    });

program
    .command('list')
    .description('List packages')
    .action(list);

program
    .command('update')
    .description('Update packages')
    .action(updatePackages);

{
    const _ = program.command('dev', { hidden: true });
    _
        .command('extract-appimage')
        .argument('<inputPath>')
        .argument('[patterns...]')
        .action(async (
            inputPath,
            patterns
        ) => extractAppimage({
            inputPath,
            patterns: patterns.length ? patterns : undefined
        }));
}

program.parse();