#!/usr/bin/env bash

. ./permissions.sh

tag=$(git tag --points-at HEAD)
hash="0.0.0-dev.$(git rev-parse --short HEAD)"
version=${tag:-$hash}

for arch in 'amd64' 'arm64'; do
  deb_path=build/dynapt-$arch

  package="Package: dynapt\n"
  package+="Version: $version\n"
  package+="Architecture: $arch\n"
  package+="Maintainer: KaKi87 <KaKi87@proton.me>\n"
  package+="Description: Dynamic APT repository\n"
  package+="Depends: bash, coreutils, dpkg, squashfs-tools, file, apt, dpkg-dev\n"

  target=""
  case $arch in
    "amd64") target="x86_64-unknown-linux-gnu";;
    "arm64") target="aarch64-unknown-linux-gnu";;
  esac

  filename="dynapt-$arch-$version"

  rm -rf $deb_path
  mkdir -p $deb_path/DEBIAN
  mkdir -p $deb_path/opt/dynapt
  mkdir -p $deb_path/usr/bin

  echo -e $package > $deb_path/DEBIAN/control

  deno compile \
    --no-prompt \
    --allow-sys=$allow_sys \
    --allow-env=$allow_env \
    --allow-run=$allow_run \
    --allow-import=$allow_import \
    --allow-read \
    --allow-write \
    --allow-net \
    --target "$target" \
    -o build/$filename \
    ./main.js \
    --is-compiled \
    --set-version="$arch $version"

  cp build/$filename $deb_path/opt/dynapt/$filename
  bash -c "cd $deb_path/usr/bin && ln -s ../../opt/dynapt/$filename ./dynapt"
  dpkg-deb --build $deb_path
done