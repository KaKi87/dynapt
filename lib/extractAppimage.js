import { readFile } from 'node:fs/promises';
import { ELFInfo } from 'npm:readelf.js';

export default async ({
    inputPath,
    outputPath,
    patterns = [
        '*.desktop',
        '*.png',
        '*.jpg',
        '*.jpeg',
        '*.svg',
        'usr/share/icons/hicolor/*/apps/*.png',
        'usr/share/icons/hicolor/*/apps/*.jpg',
        'usr/share/icons/hicolor/*/apps/*.jpeg',
        'usr/share/icons/hicolor/*/apps/*.svg'
    ]
}) => {
    const
        elfInfo = new ELFInfo(await readFile(inputPath)),
        squashfsOffset = Number(elfInfo.fileHeader.e_shoff) + elfInfo.fileHeader.e_ehsize * elfInfo.fileHeader.e_shnum,
        paths = new TextDecoder()
            .decode((await (new Deno.Command(
                'unsquashfs',
                {
                    args: [
                        '-ll',
                        '-o',
                        squashfsOffset,
                        inputPath,
                        ...patterns
                    ]
                }
            )).output()).stdout)
            .split('\n')
            .filter(_ => _.length && !_.startsWith('d'))
            .map(_ => {
                _ = _.split(/\s+/);
                return _[7] || _[5]?.slice(14);
            });

    await (new Deno.Command(
        'unsquashfs',
        {
            args: [
                '-f',
                '-o',
                squashfsOffset,
                inputPath,
                ...paths
            ],
            cwd: outputPath
        }
    )).output();

    return paths.map(_ => `squashfs-root/${_}`);
};