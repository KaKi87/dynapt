import {
    join as joinPath
} from 'https://deno.land/std@0.224.0/path/mod.ts';
import dedent from 'npm:dedent';

import extractAppimage from './extractAppimage.js';
import getBinArchitecture from './getBinArchitecture.js';
import buildDeb from './buildDeb.js';

export default async ({
    inputPath,
    outputPath,
    fallbackVersion,
    bin
}) => {
    await Deno.mkdir(outputPath, { recursive: true });

    const
        paths = await extractAppimage({
            inputPath,
            outputPath
        }),
        desktopPath = joinPath(outputPath, paths.find(_ => _.endsWith('.desktop'))),
        iconPath = joinPath(outputPath, paths.find(_ => /(png|jpe?g|svg)$/.test(_))),
        packageName = desktopPath.slice(desktopPath.lastIndexOf('/') + 1, -8).toLocaleLowerCase(),
        [desktopText] = await Promise.all([
            (async () => (await Deno.readTextFile(desktopPath))
                .replace(/Exec=[^\s]+/, `Exec=/opt/${packageName}/${packageName}.AppImage`)
                .replace(/Icon=.+/, `Icon=/opt/${packageName}/${packageName}.png`))(),
            ...[
                '/usr/share/applications',
                `/opt/${packageName}`,
                '/DEBIAN',
                ...bin ? ['/usr/bin'] : []
            ].map(_ => Deno.mkdir(joinPath(outputPath, packageName, _), { recursive: true }))
        ]),
        version = desktopText.match(/X-AppImage-Version=(.+)/)?.[1],
        architecture =
            {
                'x86_64': 'amd64',
                'aarch64': 'arm64'
            }[desktopText.match(/X-AppImage-Arch=(.+)/)?.[1]]
            ||
            await getBinArchitecture(inputPath),
        description = desktopText.match(/Comment=(.+)/)?.[1];

    await Promise.all([
        Deno.writeTextFile(joinPath(outputPath, packageName, `/usr/share/applications/${packageName}.desktop`), desktopText),
        Deno.copyFile(iconPath, joinPath(outputPath, packageName, `/opt/${packageName}/${packageName}.png`)),
        Deno.copyFile(inputPath, joinPath(outputPath, packageName, `/opt/${packageName}/${packageName}.AppImage`)),
        Deno.writeTextFile(joinPath(outputPath, packageName, '/DEBIAN/control'), dedent `
            Package: ${packageName}
            Version: ${/^[0-9]/.test(version) ? version : fallbackVersion}
            Architecture: ${architecture}
            Maintainer: unknown
            Description: ${description || 'unknown'}\n
        `),
        ...bin ? [
            new Deno.Command(
                'ln',
                {
                    args: [
                        '-s',
                        `../../opt/${packageName}/${packageName}.AppImage`,
                        `./${bin}`
                    ],
                    cwd: joinPath(outputPath, packageName, `/usr/bin`)
                }
            ).output()
        ] : []
    ]);

    return buildDeb({
        outputPath,
        name: packageName
    });
};