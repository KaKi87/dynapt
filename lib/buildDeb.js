import { join as joinPath } from 'https://deno.land/std@0.224.0/path/mod.ts';

export default async ({
    outputPath,
    name
}) => {
    const debPath = joinPath(outputPath, `${name}.deb`);

    let {
        stdout,
        stderr
    } = await new Deno.Command('dpkg-deb', { args: ['--build', joinPath(outputPath, name)] }).output();
    stdout = new TextDecoder().decode(stdout);
    stderr = new TextDecoder().decode(stderr);

    if(stdout === `dpkg-deb: building package '${name}' in '${debPath}'.\n`)
        return debPath;

    throw new Error(stderr);
};