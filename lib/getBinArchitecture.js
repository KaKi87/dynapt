export default async path => ({
    'x86-64': 'amd64',
    'aarch64': 'arm64'
})[new TextDecoder().decode((await (new Deno.Command(
    'file',
    {
        args: [path]
    }
)).output()).stdout).split(',')[1].split(' ').slice(-1)[0]];