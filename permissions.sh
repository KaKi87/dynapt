allow_sys="hostname"

allow_env="NODE_EXTRA_CA_CERTS"

allow_run="bash"
allow_run+=",dpkg-deb"
allow_run+=",ln"
allow_run+=",unsquashfs"
allow_run+=",file"
allow_run+=",apt"
allow_run+=",chmod"
allow_run+=",dpkg-scanpackages"
allow_run+=",apt-ftparchive"

allow_import="deno.land"
allow_import+=",git.kaki87.net"